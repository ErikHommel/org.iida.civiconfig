<?php
/**
 * Class for Event processing
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 17 May 2016
 * @license AGPL-3.0
 */
class CRM_Civiconfig_Event {
  private $_customFieldId = NULL;
  private $_foundEvents = array();

  /**
   * CRM_Civiconfig_Event constructor.
   */
  function __construct() {
    try {
      $customGroupId = civicrm_api3('CustomGroup', 'Getvalue', array('name' => 'iida_event_data', 'return' => 'id'));
      $customFieldParams = array(
        'custom_group_id' => $customGroupId,
        'name' => 'parent_event',
        'html_type' => 'Select',
        'return' => 'id'
      );
      try {
        $customFieldId = civicrm_api3('CustomField', 'Getvalue', $customFieldParams);
        $this->_customFieldId = (int) $customFieldId;
        $events = civicrm_api3('Event', 'Get', array('is_active' => 1));
        foreach ($events['values'] as $event) {
          $this->_foundEvents[$event['id']] = $event['title'];
        }
      } catch (CiviCRM_API3_Exception $ex) {
        throw new Exception('Could not find a custom field with name parent_event in '.__METHOD__
          .', contact your system administrator. Error from API CustomField Getvalue: '.$ex->getMessage());
      }
    } catch (CiviCRM_API3_Exception $ex) {
      throw new Exception('Could not find a custom group with name iida_event_data in '.__METHOD__
        .', contact your system administrator. Error from API CustomGroup Getvalue: '.$ex->getMessage());
    }
  }

  /**
   * Method to process customFieldOptions hook (add event data to parent_event custom field)
   * @param $fieldId
   * @param $options
   */
  public static function customFieldOptions($fieldId, &$options) {
    $event = new CRM_Civiconfig_Event();
    $options = $event->_foundEvents;
  }

}