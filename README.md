Specific CiviCRM extension for CiviCRM configuration during IIDA development project with Emphanos

This extension will create or update groups, tags, option groups, relationship types and custom groups with custom fields in the JSON files in the resources folder. 

Note: it will remove custom fields from the relevant custom groups on your CiviCRM install if they are not included in the JSON file!